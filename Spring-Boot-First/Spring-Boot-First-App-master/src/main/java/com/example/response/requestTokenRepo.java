package com.example.response;

public class requestTokenRepo {
	private long id;

	private String password;

	private String lastusername;

	public requestTokenRepo(long id, String password, String lastusername) {
		this.id = id;
		this.password = password;
		this.lastusername = lastusername;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastusername() {
		return lastusername;
	}

	public void setLastusername(String lastusername) {
		this.lastusername = lastusername;
	}
	
	
}
