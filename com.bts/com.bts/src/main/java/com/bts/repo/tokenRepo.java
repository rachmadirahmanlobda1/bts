package com.bts.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bts.model.M_token;


public interface tokenRepo extends JpaRepository<M_token, Long>  {

}
