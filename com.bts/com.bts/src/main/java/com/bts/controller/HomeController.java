package com.bts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bts.model.M_token;



@Controller
@RequestMapping(value="/api")
public class HomeController {
	@Autowired
    com.bts.repo.tokenRepo tokenrepo;
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/order/index");
		return view;
	}
	
	@GetMapping("/getuser")
    public ResponseEntity<List<M_token>> getAllOrderHeader() {
        try {
            List<M_token> orderdetail = this.tokenrepo.findAll();
            return new ResponseEntity<List<M_token>>(orderdetail, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<M_token>>(HttpStatus.NO_CONTENT);
        }
    }
	
	 @GetMapping("/getuser/{id}")
	    public ResponseEntity<?> getuserlById(@PathVariable Long id) { // public ResponseEntity<Category>
	                                                                         // getCatById(@PathVariable Long id) {
	                                                                         // dijadikan tanda ?
	        try {
	        	M_token getuserid = this.tokenrepo.findById(id).orElse(null);
	            if (getuserid != null) {
	                return new ResponseEntity<M_token>(getuserid, HttpStatus.OK);
	            } else {
	                return ResponseEntity.status(HttpStatus.NOT_FOUND)
	                        .body("Order Detail dengan id" + id + "tidak ditemukan");
	            }
	        } catch (Exception e) {
	            return new ResponseEntity<M_token>(HttpStatus.NO_CONTENT);
	        }

	    }
	@PostMapping("/token")
    public ResponseEntity<M_token> insertOrderHeader(@RequestBody M_token token) {
    	M_token tokendata = this.tokenrepo.save(token);
        if (tokendata.equals(token)) {
            return new ResponseEntity<M_token>(token, HttpStatus.OK);
        } else {
            return new ResponseEntity<M_token>(HttpStatus.NO_CONTENT);
        }
    }
    @PutMapping("/getuserdetail/{id}")
	public ResponseEntity<M_token> editCategory(@RequestBody M_token orderdetail, @PathVariable Long id) {
		try {
			orderdetail.setId(id);
			this.tokenrepo.save(orderdetail);
			return new ResponseEntity<M_token>(orderdetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<M_token>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/getuserdetail/{id}")
	public ResponseEntity<?> deleteorderheader(@PathVariable Long id) { //public ResponseEntity<Category> deleteCategory(@PathVariable Long id) { diganti tanda tanya ?
		try {
			M_token getuser = this.tokenrepo.findById(id).orElse(null);
		if (getuser != null) {
			//this.getuser.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Gagal Menghapus id" + id + "tidak ditemukan");
		}
		} catch (Exception e) {
			return new ResponseEntity<M_token>(HttpStatus.NO_CONTENT);
		}
	}
}
